Rails.application.routes.draw do

  # namespace will seperate resources to different folders. 
  # It is a good practice to keep web controllers seperate from API controllers

  # api v1 controllers are located under controllers/v1/ folder.
  namespace :v1, contraints: { subdomain: 'api' } do
    resources :users, only: [:index, :create]
    resources :brands, only: [:index, :create]
  end

end
