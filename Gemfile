source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.5'
gem 'bootstrap-sass', '~> 3.0.2.1'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.1.0'

# Utility for serializing active models
gem 'active_model_serializers', '~> 0.8.1'

# Puma is a simple, fast, threaded, and highly concurrent HTTP 1.1 server for Ruby/Rack applications. 
# Puma is intended for use in both development and production environments. 
# Puma Replace Ruby's default WebBrick server
gem 'puma', '~> 2.9.0'

# Find out which locale the user preferes by reading the languages they specified in their browser.
gem 'http_accept_language', '~> 2.0.2'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

# A simple, fast Mysql library for Ruby, binding to libmysql
# gem 'mysql2', '~> 0.3.16'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 3.1.1'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

group :development do
  gem 'sqlite3', '1.3.5'
  gem 'annotate', '~> 2.6.5'
end

# Gems used only for assets and not required in production environments by default
group :assets do 
  gem 'sass-rails',   '~> 4.0.3'
  gem 'coffee-rails', '~> 4.0.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '1.2.3'
end

group :test, :development do
  gem 'rspec-rails', '~> 3.0.2'
  gem 'guard-rspec', '~> 4.3.1'
  gem 'guard-spork', '~> 1.5.1'
  gem 'spork', '~> 0.9.2'
end

group :test do
  gem 'capybara', '~> 2.4.1'
  gem 'factory_girl_rails', '~> 4.4.1'
  gem 'cucumber-rails', '~> 1.4.1', require: false
  gem 'database_cleaner', '~> 1.3.0'
end

group :production do
  # Ruby interface to the {PostgreSQL RDBMS}
  gem 'pg', '0.12.2'
end

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

