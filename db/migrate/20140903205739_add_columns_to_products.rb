class AddColumnsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :name, :string
    add_column :products, :brand_id, :integer
  end
end
