# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(username: 'likunh1', password: '111111d', password_confirmation: '111111d', email: 'likunh1@example.com')
User.create!(username: 'likunh2', email: 'likunh2@example.com', password: '111111', password_confirmation: '111111')
User.create!(username: 'likunh3', email: 'likunh3@example.com', password: '111111', password_confirmation: '111111')
User.create!(username: 'Annabel', password: 'secret', password_confirmation: 'secret', email: 'annabel@example.com')
