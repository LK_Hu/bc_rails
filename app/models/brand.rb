# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  name       :string(255)
#

class Brand < ActiveRecord::Base
  validates :name, presence: true, uniqueness: { case_sensitive: false }
end
