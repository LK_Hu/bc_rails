# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  username              :string(255)
#  email                 :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  password_digest       :string(255)
#  password              :string(255)
#  password_confirmation :string(255)
#

class User < ActiveRecord::Base
  has_secure_password

  before_save do |user| 
    user.email.downcase! 
    user.username.downcase!
  end

  # VALID_EMAIL_REGEX is a Ruby constant, so its value can't change.
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 

  validates :username, presence: true, 
                      length: { maximum: 50 },
                      uniqueness: { case_sensitive: false }
  validates :email, presence: true, 
                    format: { with: VALID_EMAIL_REGEX},
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }
  validates :password_confirmation, presence: true

end
