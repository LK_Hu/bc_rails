module V1
  class UsersController < ApplicationController
    # null_session works for API which means that applications is stateless.
    protect_from_forgery with: :null_session

    def index
      users = User.all
      render json: users, status: 200
    end

    def create
      user = User.new(user_params)
      if user.save
        head 204
      else
        head 422
      end
    end


    private

    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end
  end
end