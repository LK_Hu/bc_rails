module V1
  class BrandsController < ApplicationController
    # null_session works for API which means that applications is stateless.
    protect_from_forgery with: :null_session

    def index
      brands = Brand.all
      render json: brands, status: 200
    end

    def create
      brand = Brand.new(brand_params)
      if brand.save
        head 204
      else
        head 422
      end
    end


    private

    def brand_params
      params.require(:brand).permit(:name)
    end

  end
end

