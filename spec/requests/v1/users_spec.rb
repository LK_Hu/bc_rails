RSpec.describe 'User', :type => :request do
  before {
    @domain = SUBDOMAIN_API_V1
    @user2 = User.create!(username: 'Annabel', password: 'secret', password_confirmation: 'secret', email: 'annabel@example.com')
    @user1 = User.create!(username: 'Annabel1', password: 'secret', password_confirmation: 'secret', email: 'annabel1@example.com')
  }

  describe "when GET /users" do
    it "response.status and response.content_type return as expected" do
      get "#{@domain}/users"
      expect(response.status).to be(200)
      expect(response.content_type).to be(Mime::JSON)
      expect(json(response.body)[:users].size).to eq(User.count)
      # raise response.body # raise will print the string on rspec output
    end
  end

  describe "when POST /users" do
    it "expect response.status 204" do
      post "#{@domain}/users", 
      {
        user: {
          username: 'Andy',
          email: 'andy@example.com',
          password: '1234567',
          password_confirmation: '1234567'
        }
      },
      {
        'Accept' => Mime::JSON, 'content_type' =>  Mime::JSON.to_s
      }
      expect(response.status).to eq(204)
      # expect(response.content_type).to eqMime::JSON)
      # expect(json(response.body)[:id]).to be_a_kind_of(Integer)
    end
  end

  describe 'when POST /users with username nil' do
    it 'expect response.status 422' do
      post "#{@domain}/users", 
      {
        user: {
          username: nil,
          email: 'andy@example.com',
          password: '1234567',
          password_confirmation: '1234567'
        }
      },
      {
        'Accept' => Mime::JSON, 'content_type' =>  Mime::JSON.to_s
      }
      expect(response.status).to eq(422)
    end
  end

end

















