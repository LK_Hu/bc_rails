require 'rails_helper'

RSpec.describe 'Brand', :type => :request do
  before {
    @domain = SUBDOMAIN_API_V1
    @brand = Brand.create!(name: 'test_brand')
  }

  subject { @brand }

  it { should be_valid }

  describe "when GET /brands" do
    it "response.status and response.content_type returns as expected" do
      get "#{@domain}/brands"
      expect(response.status).to be(200)
      expect(response.content_type).to be(Mime::JSON)
      expect(json(response.body)[:brands].size).to eq(Brand.count)
    end
  end

  describe "when POST /brands" do 
    it "expect response.status 204" do
      post "#{@domain}/brands",
      {
        brand: 
        {
          name: "ABC"
        }
      },
      {
        Accept: Mime::JSON, content_type: Mime::JSON.to_s
      }
      expect(response.status).to eq(204)
    end
  end

end
