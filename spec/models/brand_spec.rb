# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  name       :string(255)
#

require 'rails_helper'

RSpec.describe Brand, :type => :model do
  describe "Attribute" do 
    it { should respond_to(:name) }
  end

  describe "Upon Saving" do 
    before {
      @brand = Brand.create!(name: 'ABC')
    }

    describe "when Brand.name is empty" do
      before { @brand.name = '' }
      it { should_not be_valid }
    end

    describe "when Brand.name is nil" do
      before { @brand.name = nil }
      it { should_not be_valid }
    end
  end

end
