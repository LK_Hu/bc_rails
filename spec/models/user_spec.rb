# == Schema Information
#
# Table name: users
#
#  id                    :integer          not null, primary key
#  username              :string(255)
#  email                 :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  password_digest       :string(255)
#  password              :string(255)
#  password_confirmation :string(255)
#

require 'rails_helper'

RSpec.describe User, :type => :model do

  describe "Attributes" do 
    it { should respond_to (:username) }
    it { should respond_to (:email) }
    it { should respond_to (:password) }
    it { should respond_to (:password_confirmation) }
    it { should respond_to (:password_digest) }
  end

  describe "Upon saving" do 
     before { 
      @user = User.create!(username: 'Annabel', password: 'secret', password_confirmation: 'secret', email: 'annabel@example.com')
    }
    subject { @user }

    it { should be_valid }

    describe "when password is not present" do 
      before { @user.password = "" }
      xit {should_not be_valid}
    end

    describe "when password doesn't match confirmation" do 
      before { @user.password_confirmation = "" }
      it { should_not be_valid }
    end

    describe "when password is nil" do 
      before { @user.password = nil }
      it {should_not be_valid}
    end
  end

end
